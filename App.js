import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import DetailsScreen from './screens/DetailsScreen'
import ActivityIndicator from './screens/ActivityIndicator'
import Button from './screens/Button'
import DrawerLayoutAndroid from './screens/DrawerLayoutAndroid'
import FlatList from './screens/FlatList'
import Image from './screens/Image'
import KeyboardAvoidingView from './screens/KeyboardAvoidingView'
import ListView from './screens/ListView'
import Modal from './screens/Modal'
import Picker from './screens/Picker'
import ProgressBarAndroid from './screens/ProgressBarAndroid'
import RefreshControl from './screens/RefreshControl'
import ScrollView from './screens/ScrollView'
import SectionList from './screens/SectionList'
import StatusBar from './screens/StatusBar'
import Text from './screens/Text'
import TextInput from './screens/TextInput'
import TouchableHighlight from './screens/TouchableHighlight'
import TouchableNativeFeedback from './screens/TouchableNativeFeedback'
import TouchableOpacity from './screens/TouchableOpacity'
import View from './screens/View'
import ViewPagerAndroid from './screens/ViewPagerAndroid'
import WebView from './screens/WebView'
import { SafeAreaView } from 'react-native-safe-area-view';


const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    ActivityIndicator: {
      screen: ActivityIndicator,
    },
    Button: {
      screen: Button,
    },
    DrawerLayoutAndroid: {
      screen: DrawerLayoutAndroid,
    },
    FlatList: {
      screen: FlatList,
    },
    Image :{
      screen: Image,
    },
    InputAccessView: {
      screen: InputAccessView,
    },
    KeyboardAvoidingView: {
      screen: KeyboardAvoidingView,
    },
    ListView: {
      screen: ListView,
    },
    Modal: {
      screen: Modal,
    },
    Picker: {
      screen: Picker,
    },
    ProgressBarAndroid: {
      screen: ProgressBarAndroid,
    },
    RefreshControl: {
      screen: RefreshControl,
    },
    SafeAreaView: {
      screen: SafeAreaView,
    },
    ScrollView: {
      screen: ScrollView,
    },
    SectionList: {
      screen: SectionList,
    },
    StatusBar: {
      screen: StatusBar,
    },
    Text: {
      screen: Text,
    },
    TextInput: {
      screen: TextInput,
    },
    TouchableHighlight: {
      screen: TouchableHighlight,
    },
    TouchableNativeFeedback: {
      screen: TouchableNativeFeedback,
    },
    TouchableOpacity: {
      screen: TouchableOpacity,
    },
    View: {
      screen: View,
    },
    ViewPagerAndroid: {
      screen: ViewPagerAndroid,
    },
    WebView: {
      screen: WebView,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
    return <RootStack />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

