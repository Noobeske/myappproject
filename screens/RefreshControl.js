import React, { Component } from 'react';
import { ScrollView, Refresh } from "react-native";


class RefreshControl extends Component {
    constructor(props) {
      super(props);
      this.state = {
        refreshing: false,
      };
    }
  
    _onRefresh = () => {
      this.setState({refreshing: true});
      () => {
        this.setState({refreshing: false});
      };
    }
  
    render() {
      return (
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          
        />
      );
    }
  }
