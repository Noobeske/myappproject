import React, { Component } from 'react';
import {SafeAreaView } from "react-native";

export default class App extends Component {
    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
                <View style={{flex: 1}}>
                    <Text>Hello World!</Text>
               </View>
            </SafeAreaView>
        )
    }
}

