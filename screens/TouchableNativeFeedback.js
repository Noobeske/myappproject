import React, { Component } from 'react'
import { StyleSheet, TouchableNativeFeedback, Text, View,} from 'react-native'

export default class TouchableNativeFeedback extends Component {
    renderButton: function() {
    return (
        <TouchableNativeFeedback
            onPress={this._onPressButton}
            background={TouchableNativeFeedback.SelectableBackground()}>
        <View style={{width: 150, height: 100, backgroundColor: 'red'}}>
            <Text style={{margin: 30}}>Button</Text>
        </View>
        </TouchableNativeFeedback>
  );
},